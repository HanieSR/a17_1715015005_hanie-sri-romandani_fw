<?php

namespace App\Http\Controllers;

use App\BooksCategories;
use Illuminate\Http\Request;

class BooksCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BooksCategories  $booksCategories
     * @return \Illuminate\Http\Response
     */
    public function show(BooksCategories $booksCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BooksCategories  $booksCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(BooksCategories $booksCategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BooksCategories  $booksCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BooksCategories $booksCategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BooksCategories  $booksCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(BooksCategories $booksCategories)
    {
        //
    }
}
