<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::paginate(5);
        return view('books.index', compact('books'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('books.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
          'Judul' => 'required',
          'Sinopsis' => 'required|max:200',
          'penulis' => 'required',
          'publisher' => 'required',
          'harga' => 'required|numeric|max:999999',
          'stok' => 'required|numeric',
          'kategori'=> 'required',
          'cover' => 'image',
        ]);

        $buku_baru = new Book();
        $buku_baru->Judul = $request->get('Judul');
        $buku_baru->Sinopsis = $request->get('Sinopsis');
        $buku_baru->penulis = $request->get('penulis');
        $buku_baru->publisher = $request->get('publisher');
        $buku_baru->harga = $request->get('harga');
        $buku_baru->stok = $request->get('stok');
        if ($request->file('cover')) {
            $file = $request->file('cover')->store('cover','public');
            $buku_baru->cover = $file;
        }
        $buku_baru->save();

        $buku_baru->category()->attach($request->get('kategori'));

        return redirect()->route('books.index')->with('status','Book succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::findOrFail($id);
        return view('books.show',['book'=>$book]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $categories = Category::all();
        return view('books.edit',['book'=>$book, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'Judul' => 'required',
          'Sinopsis' => 'required|max:200',
          'penulis' => 'required',
          'publisher' => 'required',
          'harga' => 'required|numeric|max:999999',
          'stok' => 'required|numeric',
          'kategori'=> 'required',
        ]);

        $ubah_buku = Book::findOrFail($id);
        $ubah_buku->Judul = $request->get('Judul');
        $ubah_buku->Sinopsis = $request->get('Sinopsis');
        $ubah_buku->penulis = $request->get('penulis');
        $ubah_buku->publisher = $request->get('publisher');
        $ubah_buku->harga = $request->get('harga');
        $ubah_buku->stok = $request->get('stok');
        if ($request->file('cover')) {
            if ($ubah_buku->cover && file_exists(storage_path('storage/'.$ubah_buku->cover))) {
                Storage::delete('storage/'.$ubah_buku->cover);
            }
            $file = $request->file('cover')->store('cover','public');
            $ubah_buku->cover = $file;
        }
        $ubah_buku->save();

        $ubah_buku->category()->sync($request->get('kategori'));

        return redirect()->route('books.index',['id'=>$id])->with('status','Book succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $book = Book::findOrFail($id);
         if ($book->cover && file_exists(storage_path('public/storage/'.$book->cover))) {
             Storage::delete('public/storage/'.$book->cover);
         }
         $book->delete();
         return redirect()->route('books.index')->with('status', 'Book Successfully deleted');
     }

     public function search(Request $request)
     {
         $books = Book::when($request->found, function ($query) use ($request) {
             $query->where('Judul', 'like', "%{$request->found}%");
             // orWhere('Sinopsis', 'like', "%{$request->found}%");
         })->paginate(5);

         $books->appends($request->only('found'));

         return view('books.index', compact('books'));
     }
}
