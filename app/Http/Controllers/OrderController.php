<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(5);
        return view('orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('orders.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'invoice' => 'required|min:5|max:20',
            'totalharga' => 'required|numeric',
            'status'=> 'required'
        ]);
        $order_baru = new Order();
        $order_baru->user_id = $request->get('user_id');
        $order_baru->invoice = $request->get('invoice');
        $order_baru->totalharga = $request->get('totalharga');
        $order_baru->status = $request->get('status');
        $order_baru->save();

        return redirect()->route('orders.index')->with('status','Order Succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('users')->find($id);
        return view('orders.show',['order'=>$order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::with('users')->find($id);
        $users = User::all();
        return view('orders.edit',['order'=>$order], compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
          'invoice' => 'required|min:5|max:30',
          'user_id' => 'required',
          'totalharga' => 'required|numeric',
          'status'=> 'required'
      ]);
      $edit_baru = Order::findOrFail($id);
      $edit_baru->invoice = $request->get('invoice');
      $edit_baru->user_id = $request->get('user_id');
      $edit_baru->totalharga = $request->get('totalharga');
      $edit_baru->status = $request->get('status');
      $edit_baru->save();
      return redirect()->route('orders.index')->with('status','Order Succesfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $order = Order::findOrFail($id);
         $order->delete();
         return redirect()->route('orders.index')->with('status', 'Order Successfully deleted');
     }

     public function search(Request $request)
     {
         $orders = Order::when($request->found, function ($query) use ($request) {
           $query->where('invoice', 'like', "%{$request->found}%")->
           where('status', 'like', "%{$request->statuss}%");
         })->paginate(5);
         return view('orders.index', compact('orders'));
     }
}
