<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:5|max:25|unique:users',
            'alamat' => 'required|max:100',
            'email' => 'required|email|unique:users',
            'telepon' => 'required',
            'avatar' => 'required|image',
            'password' => 'required|min:8|max:32',
            'password_confirmation'=> 'required|same:password'
        ]);
        $pengguna_baru = new User();
        $pengguna_baru->nama = $request->get('nama');
        $pengguna_baru->alamat = $request->get('alamat');
        $pengguna_baru->email = $request->get('email');
        $pengguna_baru->telepon = $request->get('telepon');
        $pengguna_baru->status = "ACTIVE";
        $pengguna_baru->password = Hash::make($request->get('password'));
        if ($request->file('avatar')) {
            $file = $request->file('avatar')->store('avatar','public');
            $pengguna_baru->avatar = $file;
        }
        $pengguna_baru->save();
        return redirect()->route('users.index')->with('status','User Succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = User::findOrFail($id);
         return view('users.show',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $user = User::findOrFail($id);
         return view('users.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'email' => 'required|email',
          'alamat' => 'required|max:100',
          'telepon' => 'required|numeric',
          'avatar' => 'image'
        ]);

        $ubah_pengguna = User::findOrFail($id);
        $ubah_pengguna->email = $request->get('email');
        $ubah_pengguna->alamat = $request->get('alamat');
        $ubah_pengguna->telepon = $request->get('telepon');
        $ubah_pengguna->status = $request->get('status');
        if ($request->file('avatar')) {
            if ($ubah_pengguna->avatar && file_exists(storage_path('public/storage/'.$ubah_pengguna->avatar))) {
                Storage::delete('public/storage/'.$ubah_pengguna->avatar);
            }
            $file = $request->file('avatar')->store('avatar','public');
            $ubah_pengguna->avatar = $file;
        }
        $ubah_pengguna->save();
        return redirect()->route('users.index',['id'=>$id])->with('status','User succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->avatar && file_exists(storage_path('public/storage/'.$user->avatar))) {
            Storage::delete('public/storage/'.$user->avatar);
        }
        $user->delete();
        return redirect()->route('users.index')->with('status', 'User Successfully deleted');
    }

    public function search(Request $request)
    {
        // $found = $request->found;
        //
        // $users = User::where('email','LIKE', '%'.$found.'%')->paginate();
        // return view('users.index', compact('users'));
        $users = User::when($request->found, function ($query) use ($request) {
            $query->where('email', 'like', "%{$request->found}%");
        })->paginate(5);
        return view('users.index', compact('users'));
    }
}
