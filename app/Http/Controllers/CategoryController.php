<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $categories = Category::paginate(5);
        return view('categorys.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categorys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'genre' => 'required|unique:categories',
            'deskripsi' => 'required|max:200',
        ]);
        $kategori_baru = new Category();
        $kategori_baru->genre = $request->get('genre');
        $kategori_baru->deskripsi = $request->get('deskripsi');
        $kategori_baru->save();
        return redirect()->route('categories.index')->with('status','Category Succesfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('categorys.show',['category'=>$category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('categorys.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'genre' => 'required',
            'deskripsi' => 'required|max:200',
        ]);
        $edit_baru = Category::findOrFail($id);
        $edit_baru->genre = $request->get('genre');
        $edit_baru->deskripsi = $request->get('deskripsi');
        $edit_baru->save();
        return redirect()->route('categories.index',['id'=>$id])->with('status','Category succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $category = Category::findOrFail($id);
         $category->delete();
         return redirect()->route('categories.index')->with('status', 'Category Successfully deleted');
     }

     public function search(Request $request)
     {

         $categories = Category::when($request->found, function ($query) use ($request) {
            $query->where('genre', 'like', "%{$request->found}%");
         })->paginate(5);
         return view('categorys.index', compact('categories'));
     }
}
