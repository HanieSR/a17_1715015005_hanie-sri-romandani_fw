<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'totalharga', 'invoice', 'status'
    ];

    public function users() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
