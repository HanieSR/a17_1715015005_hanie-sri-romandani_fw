<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'Judul', 'Sinopsis', 'penulis', 'publisher', 'cover', 'harga', 'stok'
    ];

    public function category() {
        return $this->belongsToMany(Category::class);
    }
}
