<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'genre','deskripsi'
    ];

    public function book() {
        return $this->belongsToMany(Book::class);
    }
}
