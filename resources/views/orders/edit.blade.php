@extends('templates.home')
@section('title')
    Edit Order
@endsection
@section('content')
    <div class="container" ><br><br>
        <h3>Form Edit Order</h3>
        <hr>
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $order['invoice'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('orders.update',$order['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="invoice">Nomor Pesanan</label>
                            </div>
                            <div class="col-md-8">
                              <input type="number" class="form-control" name="invoice" id="invoice" value="{{ $order['invoice'] }}">
                              {{ ($errors->has('invoice')) ? $errors->first('invoice') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="totalharga">Total Harga</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="totalharga" id="totalharga" value="{{ $order['totalharga'] }}">
                                {{ ($errors->has('totalharga')) ? $errors->first('totalharga') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="user_id">Pengguna</label>
                            </div>
                            <div class="col-md-8">
                                <select name="user_id" id="user_id" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}">
                                    <option value="{{ $order->users->id }}">{{ $order->users->nama }}</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->nama }}</option>
                                    @endforeach
                                </select>
                                {{ ($errors->has('user_id')) ? $errors->first('user_id') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="status">Status</label>
                            </div>
                            <div class="col-md-8">
                                <select name="status" id="status" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}">
                                    <option value="{{ $order['status'] }}" >{{ $order['status'] }}</option>
                                    <option value="SUBMIT">SUBMIT</option>
                                    <option value="PROCESS">PROCESS</option>
                                    <option value="FINISH">FINISH</option>
                                    <option value="CANCEL">CANCEL</option>
                                </select>
                                {{ ($errors->has('status')) ? $errors->first('status') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
