@extends('templates.home')
@section('title')
    Detail of Order
@endsection
@section('content')
    <h1>Detail of Order </h1>
    <hr>
    <br>
    <div class="card bg-white border-info" style="max-width:70%; margin:auto; min-height:400px;">
        <div class="row">
            <div class="col-md-12 text-center">
              <br>
                <h3><strong>{{ $order['invoice'] }}</strong></h3>
            </div>
        </div>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Pengguna
            </div>
            <div class="col-md-4 col-sm-4">
                {{ $order->users->nama }}
            </div>
            <br>
        </div>
        <br>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                Total Harga
            </div>
            <div class="col-md-4 col-sm-4">
                Rp. {{ $order['totalharga'] }}
            </div>
            <br>
        </div>
        <div class="row">
            <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
              Status
            </div>
            <div class="col-md-4 col-sm-4 ">
                @if ( $order['status'] == "SUBMIT" )
                    <span class="badge badge-warning""> {{ $order['status'] }} </span>
                @elseif ( $order['status'] == "PROCESS" )
                    <span class="badge badge-primary""> {{ $order['status'] }} </span>
                @elseif ( $order['status'] == "FINISH" )
                    <span class="badge badge-success""> {{ $order['status'] }} </span>
                @else
                    <span class="badge badge-danger""> {{ $order['status'] }} </span>
                @endif
            </div>
        </div>

    </div>
@endsection
