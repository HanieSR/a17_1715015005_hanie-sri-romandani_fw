@extends('templates.home')
@section('title')
    List of Orders
@endsection
@section('css')
    <style>
        body{
            padding-top: 30px;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
        }
        td a:hover{
            text-decoration: none;
        }
        td button{
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h3>List of Orders</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-outline-primary " href="{{ route('orders.create') }}">
                    <span data-feather="plus-circle"></span>
                    Create a new order <span class="sr-only">(current)</span>
                </a>
            </div>
            <div class="col-md-8">
          			<form action="{{ route('orders.search') }}" class="form-inline" method="GET">
          			    <div class="form-group mx-sm-3 mb-2">
          			         <input class="form-control" name="found" placeholder="Search by Invoice Number..." value="{{ old('found') }}" style="width: 350px;">
          			    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <select name="statuss" id="status" class="form-control {{$errors->first('user_id') ? "is-invalid": ""}}">
                            <option value="">ANY</option>
                            <option value="SUBMIT">SUBMIT</option>
                            <option value="PROCESS">PROCESS</option>
                            <option value="FINISH">FINISH</option>
                            <option value="CANCEL">CANCEL</option>
                        </select>
                    </div>
          			    <button class="btn btn-primary mb-2" type="submit">Search</button>
          			</form>
      			</div>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">Id</th>
                        <th scope="col">Nomor Pesanan</th>
                        <th scope="col">Nama Pemesan</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order['id'] }}</td>
                            <td>{{ $order['invoice'] }}</td>
                            <td>{{ $order->users->nama }}</td>
                            <td>Rp. {{ $order['totalharga'] }}</td>
                            <td>
                                @if ( $order['status'] == "SUBMIT" )
                                    <span class="badge badge-warning"> {{ $order['status'] }} </span>
                                @elseif ( $order['status'] == "PROCESS" )
                                    <span class="badge badge-primary"> {{ $order['status'] }} </span>
                                @elseif ( $order['status'] == "FINISH" )
                                    <span class="badge badge-success"> {{ $order['status'] }} </span>
                                @else
                                    <span class="badge badge-danger"> {{ $order['status'] }} </span>
                                @endif
                            </td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ route('orders.show',$order['id']) }}">
                                <span data-feather="eye"></span>
                                Detail <span class="sr-only">(current)</span></a>
                                <a class="btn-sm btn-success d-inline" href="{{ route('orders.edit',$order['id']) }}">
                                <span data-feather="edit-2"></span>
                                Edit <span class="sr-only">(current)</span></a>
                                <form class="d-inline"
                                    onsubmit="return confirm('Delete this order permanently?')"
                                    action="{{route('orders.destroy', $order['id'])}}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                    <span data-feather="trash"></span>
                                    Delete <span class="sr-only">(current)</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="" style="margin: auto; width:150px;">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection
