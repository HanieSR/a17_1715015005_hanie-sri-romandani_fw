<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-success flex-md-nowrap p-0 shadow">
	<a class="navbar-brand" href="#">SHOP</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" datatarget="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" arialabel="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('users.index') }}">Users</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('books.index') }}">Books</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('categories.index') }}">Category</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('orders.index') }}">Orders</a>
			</li>
		</ul>

		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="text" placeholder="Search" arialabel="Search">
			<button class="btn btn-outline-danger my-2 my-sm-0" type="submit">Search</button>
		</form>

		<ul class="navbar-nav px-3">
			<li class="nav item dropdown">
			 	<a class="nav-link dropdown-toggle" href="#" id="dropdown01" datatoggle="dropdown" aria-haspopup="true" aria-expanded="false">Username</a>
			 	<div class="dropdown-menu" aria-labelledby="dropdown01">
			 		<a class="dropdown-item" href="#">Logout</a>
			 	</div>
			</li>
		</ul>
	</div>
</nav>
