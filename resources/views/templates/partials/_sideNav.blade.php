<nav class="col-md-2 d-none d-md-block bg-secondary sidebar">
	<div class="sidebar-sticky">
		<ul class="nav flex-column">
			<li class="nav-item ">
				<a class="nav-link active" href="{{ route('users.index') }}">
					<span data-feather="users"></span>Users<br>
				</a>
				<a  class="nav-link active" href="{{ route('books.index') }}">
					<span data-feather="book"></span>Books<br>
				</a>
				<a  class="nav-link active" href="{{ route('categories.index') }}">
					<span data-feather="list"></span>Category<br>
				</a>
				<a  class="nav-link active" href="{{ route('orders.index') }}">
					<span data-feather="shopping-cart"></span>Orders<br>
				</a>
			</li>
		</ul>
	</div>
</nav>
