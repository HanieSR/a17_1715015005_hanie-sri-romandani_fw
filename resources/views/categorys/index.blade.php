@extends('templates.home')
@section('title')
    List of Categories
@endsection
@section('css')
    <style>
        body{
            padding-top: 30px;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
        }
        td a:hover{
            text-decoration: none;
        }
        td button{
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h3>List of Categories</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-outline-primary " href="{{ route('categories.create') }}">
                    <span data-feather="plus-circle"></span>
                    Create a new category <span class="sr-only">(current)</span>
                </a>
            </div>
            <div class="col-md-8">
          			<form action="{{ route('categories.search') }}" class="form-inline" method="GET">
          			    <div class="form-group mx-sm-3 mb-2">
          			         <input class="form-control" name="found" placeholder="Search by name..." value="{{ old('found') }}" style="width: 550px;">
          			    </div>
          			    <button class="btn btn-primary mb-2" type="submit">Search</button>
          			</form>
      			</div>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">Id</th>
                        <th scope="col">Kategori Nama</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category['id'] }}</td>
                            <td>{{ $category['genre'] }}</td>
                            <td>{{ $category['deskripsi'] }}</td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ route('categories.show',$category['id']) }}">
                                <span data-feather="eye"></span>
                                Detail <span class="sr-only">(current)</span></a>
                                <a class="btn-sm btn-success d-inline" href="{{ route('categories.edit',$category['id']) }}">
                                <span data-feather="edit-2"></span>
                                Edit <span class="sr-only">(current)</span></a>
                                <form class="d-inline"
                                    onsubmit="return confirm('Delete this Category permanently?')"
                                    action="{{route('categories.destroy', $category['id'])}}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                    <span data-feather="trash"></span>
                                    Delete <span class="sr-only">(current)</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="" style="width:150px; margin: auto;">
            {{ $categories->links() }}
        </div>
    </div>
@endsection
