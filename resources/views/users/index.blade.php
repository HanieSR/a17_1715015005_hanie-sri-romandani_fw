@extends('templates.home')
@section('title')
    List of User
@endsection
@section('css')
    <style>
        body{
            padding-top: 30px;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
        }
        td a:hover{
            text-decoration: none;
        }
        td button{
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h3>List of User</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-outline-primary " href="{{ route('users.create') }}">
                    <span data-feather="plus-circle"></span>
                    Create new user<span class="sr-only">(current)</span>
                </a>
            </div>
            <div class="col-md-8">
          			<form action="{{ route('users.search') }}" class="form-inline" method="GET">
          			    <div class="form-group mx-sm-3 mb-2">
          			         <input class="form-control" name="found" placeholder="Search by e-mail..." value="{{ old('found') }}" style="width: 550px;">
          			    </div>
          			    <button class="btn btn-primary mb-2" type="submit">Search</button>
          			</form>
      			</div>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Telepon</th>
                        <th scope="col">Avatar</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <?php $no++ ?>
                        <tr>
                            <td>{{ $user['id'] }}</td>
                            <td>{{ $user['nama'] }}</td>
                            <td>{{ $user['email'] }}</td>
                            <td>{{ $user['telepon'] }}</td>
                            <td><img src="{{ asset('storage/'.$user['avatar']) }}" style="height:100px; width:100px;" ></td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ route('users.show',$user['id']) }}">
                                <span data-feather="eye"></span>
                                Detail <span class="sr-only">(current)</span></a>
                                <a class="btn-sm btn-success d-inline" href="{{ route('users.edit',$user['id']) }}">
                                <span data-feather="edit-2"></span>
                                Edit <span class="sr-only">(current)</span></a>
                                <form class="d-inline"
                                    onsubmit="return confirm('Delete this user permanently?')"
                                    action="{{route('users.destroy', $user['id'])}}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                    <span data-feather="trash"></span>
                                    Delete <span class="sr-only">(current)</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="" style="width: 180px; margin: auto;">
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
