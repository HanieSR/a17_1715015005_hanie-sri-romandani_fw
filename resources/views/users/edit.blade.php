@extends('templates.home')
@section('title')
    Edit User
@endsection
@section('content')
    <div class="container" >
      <!-- <br><br> -->
        <h3>Form Edit User</h3>
        <hr>
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $user['nama'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('users.update',$user['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">
                      @csrf
                      @method('PUT')
                        <div class="row">
                            <div class="col-md-3">
                                <label for="email" class="text-primary">Email</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email" id="email" value="{{ $user['email'] }}">
                                {{ ($errors->has('email')) ? $errors->first('email') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="alamat" class="text-primary">Alamat</label>
                            </div>
                            <div class="col-md-8">
                                <textarea name="alamat" class="form-control" id="alamat" cols="20" rows="5">{{ $user['alamat'] }}</textarea>
                                {{ ($errors->has('alamat')) ? $errors->first('alamat') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="telepon" class="text-primary">Telepon</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="telepon" id="telepon" value="{{ $user['telepon'] }}">
                                {{ ($errors->has('telepon')) ? $errors->first('telepon') : "" }}
                            </div>
                        </div>
                        <br>

                        <div class="row">
              						<div class="input-group mb-3">
              							<div class="col-md-3 text-primary">
              								Avatar
              							</div>

              							<div class="col-md-8">
              								<img src=" {{ asset('storage/'.$user['avatar']) }} " alt="gambar" class="img-thumbnail" height="150px" width="150px">
              								<div class="custom-file">
                									<input type="file" class="custom-file-input" name="avatar" id="newAvatar">
                									<label class="custom-file-label" for="newAvatar">Upload Avatar</label>
              									  {{ ($errors->has('avatar')) ? $errors->first('avatar') : "" }}
              								</div>
              							</div>
              						</div>
              					</div>

                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
