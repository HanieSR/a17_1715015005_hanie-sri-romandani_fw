@extends('templates.home')
@section('title')
    List of Books
@endsection
@section('css')
    <style>
        body{
            padding-top: 30px;
        }
        th, td {
            padding: 10px;
            text-align: center;
        }
        td a{
            margin: 3px;
            align-content: center;
            color: white;
        }
        td a:hover{
            text-decoration: none;
        }
        td button{
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <h3>List of Books</h3>
        <hr>

        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('status') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
            <div class="col-md-2">
                <a class="btn btn-outline-primary " href="{{ route('books.create') }}">
                    <span data-feather="plus-circle"></span>
                    Create a new book <span class="sr-only">(current)</span>
                </a>
            </div>
            <div class="col-md-8">
          			<form action="{{ route('books.search') }}" class="form-inline" method="GET">
          			    <div class="form-group mx-sm-3 mb-2">
          			         <input class="form-control" name="found" placeholder="Search by Title..." value="{{ old('found') }}" style="width: 550px;">
          			    </div>
          			    <button class="btn btn-primary mb-2" type="submit">Search</button>
          			</form>
      			</div>
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="table-primary">
                        <th scope="col">No</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Penulis</th>
                        <th scope="col">Publisher</th>
                        <th scope="col">Cover</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($books as $book)
                        <tr>
                            <td>{{ $book['id'] }}</td>
                            <td>{{ $book['Judul'] }}</td>
                            <td>{{ $book['penulis'] }}</td>
                            <td>{{ $book['publisher'] }}</td>
                            <td> <img src="{{ 'storage/'.$book['cover'] }}" alt="" width="100px" height="100px"> </td>
                            <td>
                                <a class="btn-sm btn-primary" href="{{ route('books.show',$book['id']) }}">
                                <span data-feather="eye"></span>
                                Detail <span class="sr-only">(current)</span></a>
                                <a class="btn-sm btn-success d-inline" href="{{ route('books.edit',$book['id']) }}">
                                <span data-feather="edit-2"></span>
                                Edit <span class="sr-only">(current)</span></a>
                                <form class="d-inline"
                                    onsubmit="return confirm('Delete this book permanently?')"
                                    action="{{route('books.destroy', $book['id'])}}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                    <span data-feather="trash"></span>
                                    Delete <span class="sr-only">(current)</span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="" style="margin: auto; width: 150px;">
                {{ $books->links() }}
            </div>
        </div>
    </div>
@endsection
