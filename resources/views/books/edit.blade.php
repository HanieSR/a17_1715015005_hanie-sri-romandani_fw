@extends('templates.home')
@section('Judul')
    Edit Book
@endsection
@section('content')
    <div class="container" >
        <h3>Form Edit Book</h3>
        <hr>
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5>{{ $book['Judul'] }}</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('books.update',$book['id']) }}" method="POST" class="form-group" enctype="multipart/form-data">                        @csrf
                        @method('PUT')
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="Judul">Judul</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="Judul" id="Judul" value=" {{ $book['Judul'] }}">
                                {{ ($errors->has('Judul')) ? $errors->first('Judul') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="penulis">Penulis</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="penulis" id="penulis" value=" {{ $book['penulis'] }}">
                                {{ ($errors->has('penulis')) ? $errors->first('penulis') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="publisher">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="publisher" id="publisher" value="{{ $book['publisher'] }}">
                                {{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="harga">Harga</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="harga" id="harga" value=" {{ $book['harga'] }}">
                                {{ ($errors->has('harga')) ? $errors->first('harga') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="stok">Stok</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="stok" id="stok" value="{{ $book['stok'] }}">
                                {{ ($errors->has('stok')) ? $errors->first('stok') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="deskripsi">Sinopsis</label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="form-control" name="Sinopsis" id="deskripsi" rows="6">
                                    {{ $book['Sinopsis'] }}
                                </textarea>
                                {{ ($errors->has('Sinopsis')) ? $errors->first('Sinopsis') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="kategori">Category</label>
                            </div>
                            <div class="col-md-8">
                              @foreach($book->category as $category)
                                  <span class="badge badge-primary">{{ $category->genre }}  </span>
                              @endforeach
                                <select multiple='multiple' name="kategori[]" id="kategori" class="form-control {{$errors->first('kategori') ? "is-invalid": ""}}">
                                    <option value="">Select Category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->genre }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{$errors->first('kategori')}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
              						<div class="input-group mb-3">
              							<div class="col-md-3 text-primary">
              								Cover
              							</div>

              							<div class="col-md-8">
              								<img src=" {{ asset('storage/'.$book['cover']) }} " alt="gambar" class="img-thumbnail" height="150px" width="150px">
              								<div class="custom-file">
                									<input type="file" class="custom-file-input" name="cover" id="newAvatar">
                									<label class="custom-file-label" for="newAvatar">Upload Cover</label>
              									  {{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
              								</div>
              							</div>
              						</div>
              					</div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary" >Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
