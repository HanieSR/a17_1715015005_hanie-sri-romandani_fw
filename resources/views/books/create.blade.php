@extends('templates.home')
@section('Judul')
    Create Books
@endsection
@section('content')
    <div class="container" >
      <br><br>
        <h3>Create Books</h3>
        <hr>
        <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
            <div class="card-header bg-primary text-white">
                <h5> Create a New Books</h5>
            </div>
            <div class="card-body">
                <div class="container text-primary">
                    <form action="{{ route('books.store') }}" class="form-group" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="Judul" >Judul</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="Judul" id="Judul">
                                {{ ($errors->has('Judul')) ? $errors->first('Judul') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row" >
                            <div class="col-md-3">
                                <label for="penulis">Penulis</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="penulis" id="penulis">
                                {{ ($errors->has('penulis')) ? $errors->first('penulis') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="publisher">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="publisher" id="publisher">
                                {{ ($errors->has('publisher')) ? $errors->first('publisher') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="harga">Harga</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="harga" id="harga">
                                {{ ($errors->has('harga')) ? $errors->first('harga') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="stok">Stok</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="stok" id="stok">
                                {{ ($errors->has('stok')) ? $errors->first('stok') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="Sinopsis">Sinopsis</label>
                            </div>
                            <div class="col-md-8">
                                <textarea class="form-control" name="Sinopsis" id="Sinopsis" rows="6" >

                                </textarea>
                                {{ ($errors->has('Sinopsis')) ? $errors->first('Sinopsis') : "" }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="kategori">Kategori</label>
                            </div>
                            <div class="col-md-8">
                                <select multiple='multiple' name="kategori[]" id="kategori" class="form-control {{$errors->first('kategori') ? "is-invalid": ""}}">
                                    <option value="">Select Kategori</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->genre }}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{$errors->first('kategori')}}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
              						<div class="input-group mb-3">
              							<div class="col-md-3 text-primary">
              								Cover
              							</div>

              							<div class="col-md-8">
              								<div class="custom-file">
                									<input type="file" class="custom-file-input" name="cover" id="newAvatar">
                									<label class="custom-file-label" for="newAvatar">Upload Cover</label>
              									  {{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
              								</div>
              							</div>
              						</div>
              					</div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-sm-4">
                                <button type="submit" class="btn btn-outline-primary">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
