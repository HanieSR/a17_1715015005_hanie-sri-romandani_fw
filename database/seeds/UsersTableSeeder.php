<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'Syadan Asandy Nugraha',
                'email'=>'syadan@outlook.com',
                'alamat' => 'GG Kasah 4',
                'telepon'=>'09837283782',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/18.jpg',
                'password' => Hash::make('123456')
            ],[
                'nama' => 'Hanie Sri Romandani',
                'email'=>'hanie@outlook.com',
                'alamat' => 'GG Kasah 4',
                'telepon'=>'09838827387',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/7.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'indah',
                'email'=>'indah@gmail.com',
                'alamat' => 'jl pramuka indah',
                'telepon'=>'012389712',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/1.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'rami',
                'email'=>'rami@gmail.com',
                'alamat' => 'jl pramuka rami',
                'telepon'=>'023389712',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/2.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'jihyo',
                'email'=>'jihyo@gmail.com',
                'alamat' => 'jl pramuka jihyo',
                'telepon'=>'0123838462',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/3.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'agung',
                'email'=>'agung@gmail.com',
                'alamat' => 'kongbeng',
                'telepon'=>'01098387216',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/9.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'mufli',
                'email'=>'mufli@gmail.com',
                'alamat' => 'gangbang',
                'telepon'=>'08938261736',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/10.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'yerin',
                'email'=>'yerin@gmail.com',
                'alamat' => 'jl pramuka yerin',
                'telepon'=>'02823479712',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/4.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'ray',
                'email'=>'ray@gmail.com',
                'alamat' => 'jl pramuka ray',
                'telepon'=>'0827316234',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/7.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'rizuki',
                'email'=>'rizuki@gmail.com',
                'alamat' => 'muara wahau',
                'telepon'=>'08912736721',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/8.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'astrid',
                'email'=>'astrid@gmail.com',
                'alamat' => 'loa janan',
                'telepon'=>'09281932932',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/14.jpg',
                'password' => Hash::make('123456')
            ],[
                'nama' => 'nat',
                'email'=>'nat@gmail.com',
                'alamat' => 'jl pramuka nat',
                'telepon'=>'0123485462',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/5.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'roy',
                'email'=>'roy@gmail.com',
                'alamat' => 'jl pramuka roy',
                'telepon'=>'03767826123',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/6.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'luthfi',
                'email'=>'lupi@gmail.com',
                'alamat' => 'sanga sanga',
                'telepon'=>'08478647326',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/15.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'hanie',
                'email'=>'hanie@gmail.com',
                'alamat' => 'Sambutan',
                'telepon'=>'08386128367',
                'status'=>'ACTIVE',
                'avatar'=>'avatar/11.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'zulkipil',
                'email'=>'zul@gmail.com',
                'alamat' => 'penajam',
                'telepon'=>'09898712836',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/12.jpg',
                'password' => Hash::make('123456')
            ],
            [
                'nama' => 'abduh',
                'email'=>'abduh@gmail.com',
                'alamat' => 'berau',
                'telepon'=>'08378293701',
                'status'=>'INACTIVE',
                'avatar'=>'avatar/13.jpg',
                'password' => Hash::make('123456')
            ],
        ]);
    }
}
