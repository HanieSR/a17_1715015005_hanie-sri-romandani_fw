<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
          [
              'genre' => 'Drama',
              'deskripsi' => 'Penuh dengan pembualan yang dapat membawa pada kesialan'
          ],
          [
              'genre' => 'Romance',
              'deskripsi' => 'Kisah romantis, asmara, percintaan'
          ],
          [
              'genre' => 'Adventure',
              'deskripsi' => 'Petualangan yang memberi pengetahuan dan pengalaman'
          ],
          [
              'genre' => 'Sport',
              'deskripsi' => 'Olahraga, Olahtubuh, Olahbadan, Olahjasmani, Olahrohani, Olahfisik'
          ],
          [
              'genre' => 'Family',
              'deskripsi' => 'Keluarga, seru-seruan, kehangatan'
          ],

          [
              'genre' => 'Programming',
              'deskripsi' => 'Segala bahasa pemrograman'
          ],
          [
              'genre' => 'Science',
              'deskripsi' => 'Hal magis yang dapat dinalar dan dicerna serta diserap kemudian diterima oleh manusia'
          ],
          [
              'genre' => 'Language',
              'deskripsi' => 'Bahasa seluruh dunia, kamus 1 Milyar Triliun Juta'
          ],
          [
              'genre' => 'Religion',
              'deskripsi' => 'Agama seluruh dunia ada disini, kitab-kitab, dan cerita'
          ],
          [
              'genre' => 'Constitutional Law',
              'deskripsi' => 'Semua hukum dan ketatanegaraan ada disini, hukum alam, hukum perdata, hingga hukum fiqh'
          ],[
              'genre' => 'Fantasy',
              'deskripsi' => 'Cerita penuh dengan dusta dan kebohongan agar orang berkhayal'
          ],
          [
              'genre' => 'Thriller',
              'deskripsi' => 'Menegangkan, mencekam, ketakutan yg hakiki'
          ],
          [
              'genre' => 'Comedy',
              'deskripsi' => 'seru-seruan, lucu, humor, ngakak, wkwkwk, lol'
          ],
          [
              'genre' => 'Technology',
              'deskripsi' => 'Penuh dengan hal modern dan canggih, perkembangan IT terkini'
          ],
          [
              'genre' => 'Economy n Business',
              'deskripsi' => 'Buku yg membahas mengenai ekonomi dunia yg memiliki sumber daya terbatas'
          ],
          [
              'genre' => 'Motivation',
              'deskripsi' => 'Bacotan penuh makna dari para orang sukses dunia yang menjadi motivator'
          ],
      ]);
    }
}
