<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'invoice' => 65985487,
                'totalharga' => 230000,
                'status' => 'PROCESS',
                'user_id' => 1
            ],
            [
                'invoice' => 87546895,
                'totalharga' => 420000,
                'status' => 'CANCEL',
                'user_id' => 2
            ],
            [
                'invoice' => 23454568,
                'totalharga' => 120000,
                'status' => 'PROCESS',
                'user_id' => 3
            ],[
                'invoice' => 23654568,
                'totalharga' => 150000,
                'status' => 'PROCESS',
                'user_id' => 4
            ],
            [
                'invoice' => 21457451,
                'totalharga' => 100000,
                'status' => 'FINISH',
                'user_id' => 5
            ],
            [
                'invoice' => 58458745,
                'totalharga' => 135000,
                'status' => 'SUBMIT',
                'user_id' => 6
            ],
            [
                'invoice' => 189273871,
                'totalharga' => 150000,
                'status' => 'FINISH',
                'user_id' => 7
            ],
            [
                'invoice' => 989082348,
                'totalharga' => 111000,
                'status' => 'SUBMIT',
                'user_id' => 8
            ],
            [
                'invoice' => 120098334,
                'totalharga' => 190000,
                'status' => 'FINISH',
                'user_id' => 9
            ],
            [
                'invoice' => 812736873,
                'totalharga' => 80000,
                'status' => 'SUBMIT',
                'user_id' => 10
            ],
            [
                'invoice' => 1726377737,
                'totalharga' => 400000,
                'status' => 'PROCESS',
                'user_id' => 11
            ],
            [
                'invoice' => 993889472,
                'totalharga' => 239000,
                'status' => 'PROCESS',
                'user_id' => 12
            ],
            [
                'invoice' => 387281995,
                'totalharga' => 500000,
                'status' => 'CANCEL',
                'user_id' => 13
            ],
            [
                'invoice' => 128738912,
                'totalharga' => 200000,
                'status' => 'PROCESS',
                'user_id' => 14
            ],
            [
                'invoice' => 9973612673,
                'totalharga' => 900000,
                'status' => 'CANCEL',
                'user_id' => 15
            ]
        ]);
    }
}
