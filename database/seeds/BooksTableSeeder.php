<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'Judul'=>'How To Master Your Habits',
                'Sinopsis'=>'',
                'penulis'=>'felix Y. Siauw',
                'publisher'=>'Lupa',
                'cover'=>'cover/17.jpg',
                'harga'=>'55000',
                'stok'=>'10'
            ],
            [
                'Judul'=>'PHP For Nolep',
                'penulis'=>'Chocky Gerung',
                'publisher'=>'KOM MAS',
                'harga'=>'69000',
                'stok'=>'10',
                'Sinopsis'=>'',
                'cover'=>'cover/1.jpg'
            ],
            [
                'Judul'=>'Menuju Kebangkitan Islam Dengan Pendidikan',
                'Sinopsis'=>'',
                'penulis'=>'A. Fatih Syuhada',
                'publisher'=>'lupa',
                'cover'=>'cover/16.jpg',
                'harga'=>'40000',
                'stok'=>'5'
            ],
            [
                'Judul'=>'Ternyata Ibuku Adalah Ayahku',
                'penulis'=>'Nopan And Friends',
                'publisher'=>'Mahardika',
                'harga'=>'35000',
                'stok'=>'10',
                'Sinopsis'=>'',
                'cover'=>'cover/3.jpg'
            ],
            [
                'Judul' => 'Hujan',
                'penulis' => 'Tere Liye',
                'publisher' => 'Elex Media',
                'harga' => '75000',
                'stok' => '10',
                'Sinopsis' => 'Berawal dari pertemuan Lail dengan Elijah di sebuah ruangan terapi.
                Lail menemani Elijah hanya untuk satu tujuan: ingin menghapus ingatannya tentang hujan.',
                'cover' => ''
            ],
            [
                'Judul' => 'Sang Ahli Kimia',
                'penulis' => 'Stephenie Meyer',
                'publisher' => 'mizan',
                'harga' => '60000',
                'stok' => '13',
                'Sinopsis' => 'Sebagai mantan agen, ia menyimpan rahasia tergelap agensi yang membuatnya menjadi incaran pemerintah Amerika.
                Mereka ingin ia mati. Ia hidup dalam pelarian selama hampir tiga tahun.',
                'cover' => ''
            ],
            [
                'Judul'=>'BEYOND The INSPIRATION',
                'Sinopsis'=>'',
                'penulis'=>'Felix Y. Siauw',
                'publisher'=>'Deepublish',
                'cover'=>'cover/20.jpg',
                'harga'=>'70000',
                'stok'=>'15'
            ],
            [
                'Judul' => 'Bad Boys 2',
                'penulis' => 'Nathalia Theodora',
                'publisher' => 'Elex Media',
                'harga' => '50000',
                'stok' => '30',
                'Sinopsis' => 'Memang lagi jaman ya, anak SMA jaman sekarang main geng-geng-an? Itu-lah yang sempat terlintas di benak Sophie
                Wyna, salah satu murid SMA Emerald.',
                'cover' => ''
            ],
            [
                'Judul' =>'LET ME BE WITH YOU',
                'penulis' => 'Ria N. Badaria',
                'publisher' => 'Shonen Jump',
                'harga' => '80000',
                'stok' => '15',
                'Sinopsis' => 'Tidak tahan karena terus didesak menikah oleh keluarganya. Kinanti akhirnya menerima ide gila Rivan Arya,
                sahabat kakaknya yang telah ia kenal sejak SMA. ',
                'cover' => ''
            ],
            [
                'Judul'=>'Story Of Audrey',
                'penulis'=>'Eshfand El Farsiyy',
                'publisher'=>'Iranian Publish Her',
                'harga'=>'35000',
                'stok'=>'10',
                'Sinopsis'=>'',
                'cover'=>'cover/8.jpg'
            ],
            [
                'Judul'=>'Zodiac For Life',
                'penulis'=>'Lord Umam',
                'publisher'=>'Hellyah',
                'harga'=>'12000',
                'stok'=>'10',
                'Sinopsis'=>'',
                'cover'=>'cover/12.jpg'
            ],
            [
                'Judul'=>'Jangan Pernah Menyerah Sebab Allah Bersama Kita',
                'Sinopsis'=>'',
                'penulis'=>'Aldilla D. Wijaya',
                'publisher'=>'Deepublish',
                'cover'=>'cover/19.jpg',
                'harga'=>'60000',
                'stok'=>'10'
            ],
            [
                'Judul'=>'Menguasai Efek Khusus dengan Photoshop',
                'penulis'=>'Jubilee Enterprise',
                'publisher'=>'Yuda Media',
                'harga'=>'45000',
                'stok'=>'15',
                'Sinopsis'=>'Melakukan edit foto pada photoshop',
                'cover'=>'cover/book2.jpg'
            ],
            [
                'Judul'=>'Negara Islam Dilema dan Pro Kontra',
                'Sinopsis'=>'',
                'penulis'=>'Ahmad Sarwat, LC.,MA',
                'publisher'=>'saya',
                'cover'=>'cover/18.jpg',
                'harga'=>'60000',
                'stok'=>'15'
            ],
            [
                'Judul'=>'Tuilet',
                'penulis'=>'Oben Cedric',
                'publisher'=>'PT. Grasindo',
                'harga'=>'100000',
                'stok'=>'15',
                'Sinopsis'=>'Novel bertema humor',
                'cover'=>'cover/book8.jpg'
            ],
            [
                'Judul'=>'Sabar tanpa batas',
                'penulis'=>'M. Nurroziqri',
                'publisher'=>'PT. Grasindo',
                'harga'=>'100000',
                'stok'=>'15',
                'Sinopsis'=>'sabar itu tidak ada batasnya',
                'cover'=>'cover/book14.jpg'
            ],
            [
                'Judul'=>'Kitab komik filsuf muslim',
                'penulis'=>'Ibod',
                'publisher'=>'PT. Grasindo',
                'harga'=>'100000',
                'stok'=>'15',
                'Sinopsis'=>'-',
                'cover'=>'cover/book13.jpg'
            ]
        ]);
    }
}
